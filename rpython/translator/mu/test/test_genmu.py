from rpython.rtyper.lltypesystem import lltype, rffi
from rpython.translator.interactive import Translation
from rpython.translator.mu import mutype
from rpython.translator.mu.database import MuDatabase, MuNameManager
from rpython.translator.mu.genmu import MuBundleGen, hybrid2struct


def test_factorial(tmpdir):
    def factorial(n):
        if n <= 1:
            return 1
        return n * factorial(n - 1)

    t = Translation(factorial, [lltype.Unsigned], backend='mu')
    t.compile_mu()


def test_quicksort(tmpdir):
    def swap(arr, i, j):
        t = arr[i]
        arr[i] = arr[j]
        arr[j] = t

    def partition(arr, idx_low, idx_high):
        pivot = arr[idx_high]
        i = idx_low
        for j in range(idx_low, idx_high):
            if arr[j] < pivot:
                swap(arr, i, j)
                i += 1
        swap(arr, i, idx_high)
        return i

    def quicksort(arr, start, end):
        if start < end:
            p = partition(arr, start, end)
            quicksort(arr, start, p - 1)
            quicksort(arr, p + 1, end)

    t = Translation(quicksort, [rffi.CArrayPtr(rffi.LONGLONG), lltype.Signed, lltype.Signed], backend='mu')
    t.compile_mu()


def test_bigintbenchmark(tmpdir):
    from rpython.translator.goal.targetbigintbenchmark import entry_point
    t = Translation(entry_point, [str], backend='mu', codegen='c')
    t.compile_mu()


def test_ovf():
    from rpython.rlib.rarithmetic import ovfcheck
    from rpython.translator.mu.test.test_mutyper import graph_of
    def f(a, b):
        try:
            return ovfcheck(a + b)
        except OverflowError:
            raise MemoryError

    t = Translation(f, [int, int], backend='mu')
    t.compile_mu()
    graph_f = graph_of(f, t)
    # graph_f.view()


def test_no_ovf_flag():
    from rpython.rlib.rarithmetic import ovfcheck
    from rpython.translator.mu.test.test_mutyper import graph_of
    def f(a, b):
        try:
            return ovfcheck(a + b)
        except OverflowError:
            raise MemoryError

    t = Translation(f, [int, int], backend='mu', no_ovf=True)
    t.compile_mu()
    graph_f = graph_of(f, t)
    # graph_f.view()


def test_init_heap():
    class A:
        def __init__(self, s):
            self.s = s

        def __str__(self):
            return self.s + '_suffix'

    a = A("string")

    def f():
        return str(a)

    t = Translation(f, [], backend='mu')
    t.compile_mu()


def test_weakref():
    import weakref
    class A:
        pass

    a1 = A()
    a1.hello = 5
    w1 = weakref.ref(a1)
    a2 = A()
    a2.hello = 8
    w2 = weakref.ref(a2)

    def f(n):
        if n:
            r = w1
        else:
            r = w2
        return r().hello

    t = Translation(f, [int], backend='mu')
    t.compile_mu()


def test_hybrid2struct():
    from rpython.translator.mu.ll2mu import LL2MuMapper
    from rpython.rtyper.lltypesystem.rstr import STR

    Hyb = LL2MuMapper().map_type(STR)
    hyb = mutype.newhybrid(Hyb, 5)._obj
    h = mutype.mu_int64(hash(hyb))
    hyb.gc_idhash = h
    hyb.hash = h
    hyb.length = mutype.mu_int64(5)
    for i, c in enumerate("hello"):
        hyb.chars[i] = mutype.mu_int8(ord(c))

    stt = hybrid2struct(hyb)
    assert isinstance(mutype.mutypeOf(stt), mutype.MuStruct)
    assert stt.gc_idhash == h
    assert stt.hash == h
    assert stt.length == mutype.mu_int64(5)
    for i, c in enumerate("hello"):
        assert stt.chars[i] == mutype.mu_int8(ord(c))

# C alternatives for RPython printing and str2int conversion, can simplify bundle
def print_(s):
    from rpython.rlib.rposix import c_write
    s += '\n'
    c_write(rffi.cast(rffi.INT, 1),
            rffi.cast(rffi.CCHARP, rffi.str2charp(s)),
            rffi.cast(rffi.SIZE_T, len(s)))
c_atoi = rffi.llexternal('atoi', [rffi.CCHARP], rffi.INT, _nowrapper=True)
int_ = lambda s: rffi.cast(rffi.LONGLONG, c_atoi(rffi.str2charp(s)))

def test_reloc():
    import py
    from rpython.translator.platform import platform
    from rpython.translator.platform import log as log_platform
    from rpython.rtyper.lltypesystem.ll_str import hex_chars

    def entry_point(argv):
        print_(hex_chars[int_(argv[1])])
        return 0

    t = Translation(entry_point, None, backend='mu',
                    impl='holstein', codegen='c',
                    vmargs='automagicReloc=False')

    t.driver.exe_name = '/tmp/test_reloc-mu'
    db, _, _ = t.compile_mu()

    exe = py.path.local(t.driver.exe_name)

    # Zebu
    # exe.chmod(stat.S_IRWXU)
    # Holstein
    from rpython.rlib.rmu import holstein
    runmu = py.path.local(holstein.mu_dir).join('..', 'tools', 'runmu.sh')
    flags = ['--vmLog=ERROR']

    cmdargs = [str(10)]

    # Zebu
    # res = platform.execute(str(exe), cmdargs,
    #                        compilation_info=rffi.ExternalCompilationInfo(
    #                            library_dirs=[db.libsupport_path.dirpath().strpath]))

    # Holstein
    log_platform.execute(' '.join([str(runmu)] + flags + [str(exe)] + cmdargs))
    res = platform.execute(runmu, flags + [str(exe)] + cmdargs)

    assert res.returncode == 0, res.err
    # assert res.out == 'key = %d\n%s' % (k, hex(k)) + '\n'
    assert res.out == "%s\n" % hex(10)[2:]


def test_debug():
    import py, stat
    from rpython.rlib.rposix import c_exit, c_write
    from rpython.translator.platform import platform
    from rpython.translator.platform import log as log_platform
    from rpython.rtyper.lltypesystem import rffi, lltype
    from rpython.rtyper.lltypesystem.lloperation import llop

    def my_print(s):
        s += '\n'
        c_write(rffi.cast(rffi.INT, 1),
                rffi.cast(rffi.CCHARP, rffi.str2charp(s)),
                rffi.cast(rffi.SIZE_T, len(s)))

    PI64 = rffi.CArrayPtr(rffi.LONGLONG)
    def raw_get_length(s):
        from rpython.rlib.objectmodel import keepalive_until_here
        adr = rffi.cast_ptr_to_adr(s)
        p = rffi.cast(PI64, adr)
        length = p[2]
        keepalive_until_here(s)
        return length

    c_atoi = rffi.llexternal('atoi', [rffi.CCHARP], rffi.INT, _nowrapper=True)

    rg = range(256, 4096)
    test_dict = dict(map(lambda x: (x, hex(x)), rg))
    test_list = [hex(x) for x in rg]
    def entry_point_raw(argc, argv):
        llop.mu_threadlocalref_init(lltype.Void)
        key = rffi.cast(rffi.LONGLONG, c_atoi(argv[1]))
        # print "key = %d" % key
        s = test_list[key]
        my_print(str(raw_get_length(s)))
        my_print(s)
        c_exit(rffi.cast(rffi.INT, 0))
        return 0

    def entry_point(argv):
        print test_dict[int(argv[1])]
        return 0

    t = Translation(entry_point, None, backend='mu',
                    impl='holstein', codegen='c')
    # t = Translation(entry_point_raw, [rffi.INT, rffi.CCHARPP], backend='mu',
    #                 impl='zebu', codegen='c', no_ovf=True)

    # t.driver.standalone = True  # force standalone
    # t.driver.disable(['entrypoint_mu'])
    t.driver.exe_name = 'test_debug-%(backend)s'

    db, _, _ = t.compile_mu()

    exe = py.path.local('test_debug-mu')

    # Zebu
    # exe.chmod(stat.S_IRWXU)
    # Holstein
    from rpython.rlib.rmu import holstein
    runmu = py.path.local(holstein.mu_dir).join('..', 'tools', 'runmu.sh')
    flags = ['--vmLog=ERROR']

    for i in range(10):
        from random import randint
        k = randint(rg[0], rg[-1])
        cmdargs = [str(k)]

        # Zebu
        # res = platform.execute(str(exe), cmdargs,
        #                        compilation_info=rffi.ExternalCompilationInfo(
        #                            library_dirs=[db.libsupport_path.dirpath().strpath]))

        # Holstein
        log_platform.execute(' '.join([str(runmu)] + flags + [str(exe)] + cmdargs))
        res = platform.execute(runmu, flags + [str(exe)] + cmdargs)

        assert res.returncode == 0, res.err
        # assert res.out == 'key = %d\n%s' % (k, hex(k)) + '\n'
        assert res.out == "%s\n" % test_dict[k]


def holstein_run(exe_name, cmdargs, flags=['--vmLog=ERROR']):
    import py
    from rpython.translator.platform import platform
    from rpython.translator.platform import log as log_platform
    from rpython.rlib.rmu import holstein

    exe = py.path.local(exe_name)
    runmu = py.path.local(holstein.mu_dir).join('..', 'tools', 'runmu.sh')

    log_platform.execute(' '.join([str(runmu)] + flags + [str(exe)] + cmdargs))
    res = platform.execute(runmu, flags + [str(exe)] + cmdargs)
    return res


def test_tlref():
    from rpython.rlib.rthread import ThreadLocalReference
    class FooBar(object):
        pass

    t = ThreadLocalReference(FooBar)

    def main(argv):
        x1 = FooBar()
        t.set(x1)
        return int(t.get() is x1)

    config = {
        'backend': 'mu',
        'impl': 'holstein',
        'codegen': 'c'
    }

    t = Translation(main, None, **config)
    t.driver.exe_name = 'test_tlref-%(backend)s'
    db, _, _ = t.compile_mu()

    res = holstein_run(t.driver.exe_name % config, [])

    assert res.returncode == 1, res.err
    # assert res.out == 'key = %d\n%s' % (k, hex(k)) + '\n'
