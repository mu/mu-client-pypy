from rpython.translator.interactive import Translation
from rpython.translator.mu.opt import boolflag
from rpython.translator.mu.test.test_mutyper import graph_of
from rpython.translator.mu.tool.textgraph import print_graph


def test_boolflag():
    def f(x):
        if x > 1:
            return 3
        return 0

    t = Translation(f, [int], backend='mu')
    t.mutype()
    g = graph_of(f, t)
    print_graph(g)
    assert boolflag._has_pattern(g.startblock)
    boolflag.optimise([g])
    assert not boolflag._has_pattern(g.startblock)
    assert [op.opname for op in g.startblock.operations] == ['mu_cmpop', 'mu_branch2']
    ops = g.startblock.operations
    assert ops[1].args[0] is ops[0].result
