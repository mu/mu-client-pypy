"""
Optimise on the pattern:
    %cmp_res = CMPOP %a %b
    %res = ZEXT <@i8> %cmp_res
    %con = EQ %res  @1_i8
    BRANCH2 %con %blk1(...) %blk2(...)
by removing the SELECT and EQ instructions.
"""


def _has_pattern(blk):
    try:
        ops = blk.operations
        assert len(ops) >= 4

        last4 = ops[-4:]
        assert last4[0].opname == 'mu_cmpop'
        assert last4[1].opname == 'mu_convop' and last4[1].args[0].value == 'ZEXT'
        assert last4[2].opname == 'mu_cmpop' and last4[2].args[0].value == 'EQ'
        assert last4[3].opname == 'mu_branch2'

        op_cmp, op_zext, op_eq, op_branch2 = last4
        assert op_cmp.result is op_zext.args[2]
        assert op_zext.result is op_eq.args[1]
        assert op_eq.result is op_branch2.args[0]

        return True
    except AssertionError:
        return False


def optimise(graphs):
    for g in graphs:
        for blk in g.iterblocks():
            if _has_pattern(blk):
                ops = blk.operations
                br2 = ops[-1]
                br2.args[0] = ops[-4].result
                blk.operations = ops[:-3] + [ops[-1]]
