from rpython.rlib.rmu import holstein_meta as rmu
from rpython.translator.interactive import Translation
from rpython.translator.mu.tool.textgraph import print_graph
from rpython.translator.mu.test.test_mutyper import graph_of
from rpython.translator.mu import mutype, ll2mu
from rpython.rtyper.lltypesystem import lltype, rffi


def test_str2metacstr():
    t = Translation(rmu.str2metacstr, [str], backend='mu')
    t.backendopt(mallocs=True, inline=True)
    g = graph_of(rmu.str2metacstr, t)
    print_graph(g)
    t.mutype()
    print_graph(g)
    blk = g.startblock.exits[1].target
    assert [op.opname for op in blk.operations[-3:-1]] == ['mu_getiref', 'mu_getvarpartiref']
    assert isinstance(blk.operations[-3].result.concretetype, mutype.MuIRef)


def test_str2metabytes():
    t = Translation(rmu.str2metabytes, [str], backend='mu')
    t.backendopt(mallocs=True, inline=True)
    g = graph_of(rmu.str2metabytes, t)
    print_graph(g)
    t.mutype()
    print_graph(g)
    assert g.returnblock.inputargs[0].concretetype == mutype.META_BYTES_REF


def test_lst2metaarr():
    def f(lst):
        return rmu.lst2metacarr(rmu.MuID, lst)
    t = Translation(f, [[rmu.MuID]], backend='mu')
    t.annotate()
    g = graph_of(f, t)
    t.backendopt(mallocs=True, inline=True)
    t.mutype()

    print_graph(g)

    assert [op.opname for op in g.startblock.operations[:-1]] == \
           ['mu_getiref',
            'mu_getfieldiref',
            'mu_load',
            'mu_getiref',
            'mu_getvarpartiref',
            'mu_shiftiref']


def test_type_translation():
    def main(argv):
        bldr = rmu.MuIRBuilder()
        i8 = bldr.gen_sym("@i8")
        bldr.new_type_int(i8, 8)
        stt = bldr.gen_sym("@stt")
        bldr.new_type_struct(stt, [i8])
        bldr.load()
        return 0

    t = Translation(main, None, backend='mu')
    t.annotate()
    # t.view()
    t.backendopt(mallocs=True, inline=True)
    g = graph_of(main, t)
    g.view()
    print_graph(g)
    t.mutype()
    g.view()
    print_graph(g)


def test_func_by_id():
    FUNCPTR = lltype.Ptr(lltype.FuncType([lltype.Signed], lltype.Signed))
    def f(id):
        return rmu.meta_func_by_id(FUNCPTR, id)

    t = Translation(f, [rmu.MuID], backend='mu')
    t.rtype()
    h = graph_of(rmu.meta_func_by_id, t)
    assert h.startblock.operations[0].opname == 'mu_meta_xxx_by_id'
    t.mutype()
    g = graph_of(f, t)
    print_graph(g)

    op = g.startblock.operations[0]
    meta_info = op.args[-1].value
    assert 'types' in meta_info
    assert meta_info['types'] == [ll2mu.ll2mu_type(FUNCPTR)]


def test_call_meta_compiled_func():
    import py

    def build_func(bldr):
        """
        Builds the following test bundle.
            .const @c_1 <@i64> = 1
            .funcsig @sig_i64_i64 = (@i64) -> (i@64)
            .func @inc VERSION @inc.v1 <@sig_i64_i64> {
                %blk0(<@i64> %a):
                    %a_suc = ADD <@i64> %a @c_1
                    RET %a_suc
            }
        :type bldr: rpython.rlib.rmu.holstein_meta.MuIRBuilder
        """
        i64 = rmu.meta_id_of("@i64")
        c_1 = bldr.gen_sym("@c_1"); bldr.new_const_int(c_1, i64, 1)
        sig_i64_i64 = rmu.meta_id_of("@sig_i64_i64")    # assume known signature name
        # sig_i64_i64 = bldr.gen_sym("@sig_i64_i64_meta")  # alternate approach, redefine a signature with another name
        # bldr.new_funcsig(sig_i64_i64, [i64], [i64])
        stt = bldr.gen_sym(); bldr.new_type_struct(stt, [i64])
        inc = bldr.gen_sym("@inc"); bldr.new_func(inc, sig_i64_i64)

        blk0 = bldr.gen_sym("@inc.blk0")
        a = bldr.gen_sym("@inc.blk0.a")
        a_suc = bldr.gen_sym("@inc.blk0.a_suc")
        op_add = bldr.gen_sym(); bldr.new_binop(op_add, a_suc, rmu.MuBinOptr.ADD, i64, a, c_1)
        op_ret = bldr.gen_sym(); bldr.new_ret(op_ret, [a_suc])
        bldr.new_bb(blk0, [a], [i64], rmu.MU_NO_ID, [op_add, op_ret])
        bldr.new_func_ver(bldr.gen_sym(), inc, [blk0])
        return inc

    FUNCPTR = lltype.Ptr(lltype.FuncType([lltype.Signed], lltype.Signed))

    def main(args):
        bldr = rmu.MuIRBuilder()
        id_fnc = build_func(bldr)
        bldr.load()
        a = int(args[1])
        inc = rmu.meta_func_by_id(FUNCPTR, id_fnc)
        a_inc = inc(a)
        print a_inc
        return 0

    t = Translation(main, None, backend='mu', impl='holstein', codegen='c')
    t.driver.standalone = True  # force standalone
    t.driver.exe_name = '/tmp/test_compile_calls-mu'
    t.annotate()
    # t.backendopt(mallocs=True)
    t.mutype()
    t.compile_mu()

    img = py.path.local(t.driver.exe_name)

    from rpython.rlib.rmu import holstein
    from rpython.translator.platform import platform, log as log_platform

    runmu = py.path.local(holstein.mu_dir).join('..', 'tools', 'runmu.sh')
    flags = ['--vmLog=ERROR', '--losSize=780M', '--sosSize=780M']
    args = ['42']
    log_platform.execute(' '.join([str(runmu)] + flags + [str(img)] + args))
    res = platform.execute(runmu, flags + [str(img)] + args)
    assert res.returncode == 0, res.err
    assert res.out == '43\n'
