"""
Meta-circular bundle building interface
"""

from rpython.rtyper.lltypesystem import rffi, lltype, llmemory, rstr
from rpython.rtyper.lltypesystem.lloperation import llop
from rpython.rlib.objectmodel import specialize, always_inline, ExtRegistryEntry, keepalive_until_here
from rpython.translator.mu import mutype
from rpython.translator.mu.ll2mu import ll2mu_type


IRBuilderRef = lltype.Ptr(lltype.GcOpaqueType('irbuilderref', hints={'mu_render_ptr_as': mutype.MUTA_IRBUILDER_REF}))
MuID = rffi.UINT     # MU_INT32
MuFlag = rffi.UINT
MU_NO_ID = rffi.cast(MuID, 0)


class MetaInst:
    ID_OF = "META_ID_OF"
    NAME_OF = "META_NAME_OF"
    LOAD_BUNDLE = "META_LOAD_BUNDLE"
    LOAD_HAIL = "META_LOAD_HAIL"
    NEW_CURSOR = "META_NEW_CURSOR"
    NEXT_FRAME = "META_NEXT_FRAME"
    COPY_CURSOR = "META_COPY_CURSOR"
    CLOSE_CURSOR = "META_CLOSE_CURSOR"
    CUR_FUNC = "META_CUR_FUNC"
    CUR_FUNC_VER = "META_CUR_FUNC_VER"
    CUR_INST = "META_CUR_INST"
    DUMP_KEEPALIVES = "META_DUMP_KEEPALIVES"
    POP_FRAMES_TO = "META_POP_FRAMES_TO"
    PUSH_FRAME = "META_PUSH_FRAME"
    ENABLE_WATCHPOINT = "META_ENABLE_WATCHPOINT"
    DISABLE_WATCHPOINT = "META_DISABLE_WATCHPOINT"
    SET_TRAP_HANDLER = "META_SET_TRAP_HANDLER"
    CONSTANT_BY_ID = "META_CONSTANT_BY_ID"
    GLOBAL_BY_ID = "META_GLOBAL_BY_ID"
    FUNC_BY_ID = "META_FUNC_BY_ID"
    EXPFUNC_BY_ID = "META_EXPFUNC_BY_ID"


class MetaIRBuilderInst:
    NEW_IR_BUILDER = "IRBUILDER_NEW_IR_BUILDER"
    LOAD = "IRBUILDER_LOAD"
    ABORT = "IRBUILDER_ABORT"
    GEN_SYM = "IRBUILDER_GEN_SYM"
    NEW_TYPE_INT = "IRBUILDER_NEW_TYPE_INT"
    NEW_TYPE_FLOAT = "IRBUILDER_NEW_TYPE_FLOAT"
    NEW_TYPE_DOUBLE = "IRBUILDER_NEW_TYPE_DOUBLE"
    NEW_TYPE_UPTR = "IRBUILDER_NEW_TYPE_UPTR"
    NEW_TYPE_UFUNCPTR = "IRBUILDER_NEW_TYPE_UFUNCPTR"
    NEW_TYPE_STRUCT = "IRBUILDER_NEW_TYPE_STRUCT"
    NEW_TYPE_HYBRID = "IRBUILDER_NEW_TYPE_HYBRID"
    NEW_TYPE_ARRAY = "IRBUILDER_NEW_TYPE_ARRAY"
    NEW_TYPE_VECTOR = "IRBUILDER_NEW_TYPE_VECTOR"
    NEW_TYPE_VOID = "IRBUILDER_NEW_TYPE_VOID"
    NEW_TYPE_REF = "IRBUILDER_NEW_TYPE_REF"
    NEW_TYPE_IREF = "IRBUILDER_NEW_TYPE_IREF"
    NEW_TYPE_WEAKREF = "IRBUILDER_NEW_TYPE_WEAKREF"
    NEW_TYPE_FUNCREF = "IRBUILDER_NEW_TYPE_FUNCREF"
    NEW_TYPE_TAGREF64 = "IRBUILDER_NEW_TYPE_TAGREF64"
    NEW_TYPE_THREADREF = "IRBUILDER_NEW_TYPE_THREADREF"
    NEW_TYPE_STACKREF = "IRBUILDER_NEW_TYPE_STACKREF"
    NEW_TYPE_FRAMECURSORREF = "IRBUILDER_NEW_TYPE_FRAMECURSORREF"
    NEW_TYPE_IRBUILDERREF = "IRBUILDER_NEW_TYPE_IRBUILDERREF"
    NEW_FUNCSIG = "IRBUILDER_NEW_FUNCSIG"
    NEW_CONST_INT = "IRBUILDER_NEW_CONST_INT"
    NEW_CONST_INT_EX = "IRBUILDER_NEW_CONST_INT_EX"
    NEW_CONST_FLOAT = "IRBUILDER_NEW_CONST_FLOAT"
    NEW_CONST_DOUBLE = "IRBUILDER_NEW_CONST_DOUBLE"
    NEW_CONST_NULL = "IRBUILDER_NEW_CONST_NULL"
    NEW_CONST_SEQ = "IRBUILDER_NEW_CONST_SEQ"
    NEW_CONST_EXTERN = "IRBUILDER_NEW_CONST_EXTERN"
    NEW_GLOBAL_CELL = "IRBUILDER_NEW_GLOBAL_CELL"
    NEW_FUNC = "IRBUILDER_NEW_FUNC"
    NEW_EXP_FUNC = "IRBUILDER_NEW_EXP_FUNC"
    NEW_FUNC_VER = "IRBUILDER_NEW_FUNC_VER"
    NEW_BB = "IRBUILDER_NEW_BB"
    NEW_DEST_CLAUSE = "IRBUILDER_NEW_DEST_CLAUSE"
    NEW_EXC_CLAUSE = "IRBUILDER_NEW_EXC_CLAUSE"
    NEW_KEEPALIVE_CLAUSE = "IRBUILDER_NEW_KEEPALIVE_CLAUSE"
    NEW_CSC_RET_WITH = "IRBUILDER_NEW_CSC_RET_WITH"
    NEW_CSC_KILL_OLD = "IRBUILDER_NEW_CSC_KILL_OLD"
    NEW_NSC_PASS_VALUES = "IRBUILDER_NEW_NSC_PASS_VALUES"
    NEW_NSC_THROW_EXC = "IRBUILDER_NEW_NSC_THROW_EXC"
    NEW_BINOP = "IRBUILDER_NEW_BINOP"
    NEW_BINOP_WITH_STATUS = "IRBUILDER_NEW_BINOP_WITH_STATUS"
    NEW_CMP = "IRBUILDER_NEW_CMP"
    NEW_CONV = "IRBUILDER_NEW_CONV"
    NEW_SELECT = "IRBUILDER_NEW_SELECT"
    NEW_BRANCH = "IRBUILDER_NEW_BRANCH"
    NEW_BRANCH2 = "IRBUILDER_NEW_BRANCH2"
    NEW_SWITCH = "IRBUILDER_NEW_SWITCH"
    NEW_CALL = "IRBUILDER_NEW_CALL"
    NEW_TAILCALL = "IRBUILDER_NEW_TAILCALL"
    NEW_RET = "IRBUILDER_NEW_RET"
    NEW_THROW = "IRBUILDER_NEW_THROW"
    NEW_EXTRACTVALUE = "IRBUILDER_NEW_EXTRACTVALUE"
    NEW_INSERTVALUE = "IRBUILDER_NEW_INSERTVALUE"
    NEW_EXTRACTELEMENT = "IRBUILDER_NEW_EXTRACTELEMENT"
    NEW_INSERTELEMENT = "IRBUILDER_NEW_INSERTELEMENT"
    NEW_SHUFFLEVECTOR = "IRBUILDER_NEW_SHUFFLEVECTOR"
    NEW_NEW = "IRBUILDER_NEW_NEW"
    NEW_NEWHYBRID = "IRBUILDER_NEW_NEWHYBRID"
    NEW_ALLOCA = "IRBUILDER_NEW_ALLOCA"
    NEW_ALLOCAHYBRID = "IRBUILDER_NEW_ALLOCAHYBRID"
    NEW_GETIREF = "IRBUILDER_NEW_GETIREF"
    NEW_GETFIELDIREF = "IRBUILDER_NEW_GETFIELDIREF"
    NEW_GETELEMIREF = "IRBUILDER_NEW_GETELEMIREF"
    NEW_SHIFTIREF = "IRBUILDER_NEW_SHIFTIREF"
    NEW_GETVARPARTIREF = "IRBUILDER_NEW_GETVARPARTIREF"
    NEW_LOAD = "IRBUILDER_NEW_LOAD"
    NEW_STORE = "IRBUILDER_NEW_STORE"
    NEW_CMPXCHG = "IRBUILDER_NEW_CMPXCHG"
    NEW_ATOMICRMW = "IRBUILDER_NEW_ATOMICRMW"
    NEW_FENCE = "IRBUILDER_NEW_FENCE"
    NEW_TRAP = "IRBUILDER_NEW_TRAP"
    NEW_WATCHPOINT = "IRBUILDER_NEW_WATCHPOINT"
    NEW_WPBRANCH = "IRBUILDER_NEW_WPBRANCH"
    NEW_CCALL = "IRBUILDER_NEW_CCALL"
    NEW_NEWTHREAD = "IRBUILDER_NEW_NEWTHREAD"
    NEW_SWAPSTACK = "IRBUILDER_NEW_SWAPSTACK"
    NEW_COMMINST = "IRBUILDER_NEW_COMMINST"

# --------------------
# Flags
class MuBinOpStatus:
    N = rffi.cast(MuFlag, 0x01)
    Z = rffi.cast(MuFlag, 0x02)
    C = rffi.cast(MuFlag, 0x04)
    V = rffi.cast(MuFlag, 0x08)
class MuBinOptr:
    ADD = rffi.cast(MuFlag, 0x01)
    SUB = rffi.cast(MuFlag, 0x02)
    MUL = rffi.cast(MuFlag, 0x03)
    SDIV = rffi.cast(MuFlag, 0x04)
    SREM = rffi.cast(MuFlag, 0x05)
    UDIV = rffi.cast(MuFlag, 0x06)
    UREM = rffi.cast(MuFlag, 0x07)
    SHL = rffi.cast(MuFlag, 0x08)
    LSHR = rffi.cast(MuFlag, 0x09)
    ASHR = rffi.cast(MuFlag, 0x0A)
    AND = rffi.cast(MuFlag, 0x0B)
    OR = rffi.cast(MuFlag, 0x0C)
    XOR = rffi.cast(MuFlag, 0x0D)
    FADD = rffi.cast(MuFlag, 0xB0)
    FSUB = rffi.cast(MuFlag, 0xB1)
    FMUL = rffi.cast(MuFlag, 0xB2)
    FDIV = rffi.cast(MuFlag, 0xB3)
    FREM = rffi.cast(MuFlag, 0xB4)
class MuCmpOptr:
    EQ = rffi.cast(MuFlag, 0x20)
    NE = rffi.cast(MuFlag, 0x21)
    SGE = rffi.cast(MuFlag, 0x22)
    SGT = rffi.cast(MuFlag, 0x23)
    SLE = rffi.cast(MuFlag, 0x24)
    SLT = rffi.cast(MuFlag, 0x25)
    UGE = rffi.cast(MuFlag, 0x26)
    UGT = rffi.cast(MuFlag, 0x27)
    ULE = rffi.cast(MuFlag, 0x28)
    ULT = rffi.cast(MuFlag, 0x29)
    FFALSE = rffi.cast(MuFlag, 0xC0)
    FTRUE = rffi.cast(MuFlag, 0xC1)
    FUNO = rffi.cast(MuFlag, 0xC2)
    FUEQ = rffi.cast(MuFlag, 0xC3)
    FUNE = rffi.cast(MuFlag, 0xC4)
    FUGT = rffi.cast(MuFlag, 0xC5)
    FUGE = rffi.cast(MuFlag, 0xC6)
    FULT = rffi.cast(MuFlag, 0xC7)
    FULE = rffi.cast(MuFlag, 0xC8)
    FORD = rffi.cast(MuFlag, 0xC9)
    FOEQ = rffi.cast(MuFlag, 0xCA)
    FONE = rffi.cast(MuFlag, 0xCB)
    FOGT = rffi.cast(MuFlag, 0xCC)
    FOGE = rffi.cast(MuFlag, 0xCD)
    FOLT = rffi.cast(MuFlag, 0xCE)
    FOLE = rffi.cast(MuFlag, 0xCF)
class MuConvOptr:
    TRUNC = rffi.cast(MuFlag, 0x30)
    ZEXT = rffi.cast(MuFlag, 0x31)
    SEXT = rffi.cast(MuFlag, 0x32)
    FPTRUNC = rffi.cast(MuFlag, 0x33)
    FPEXT = rffi.cast(MuFlag, 0x34)
    FPTOUI = rffi.cast(MuFlag, 0x35)
    FPTOSI = rffi.cast(MuFlag, 0x36)
    UITOFP = rffi.cast(MuFlag, 0x37)
    SITOFP = rffi.cast(MuFlag, 0x38)
    BITCAST = rffi.cast(MuFlag, 0x39)
    REFCAST = rffi.cast(MuFlag, 0x3A)
    PTRCAST = rffi.cast(MuFlag, 0x3B)
class MuMemOrd:
    NOT_ATOMIC = rffi.cast(MuFlag, 0x00)
    RELAXED = rffi.cast(MuFlag, 0x01)
    CONSUME = rffi.cast(MuFlag, 0x02)
    ACQUIRE = rffi.cast(MuFlag, 0x03)
    RELEASE = rffi.cast(MuFlag, 0x04)
    ACQ_REL = rffi.cast(MuFlag, 0x05)
    SEQ_CST = rffi.cast(MuFlag, 0x06)
class MuAtomicRMWOptr:
    XCHG = rffi.cast(MuFlag, 0x00)
    ADD = rffi.cast(MuFlag, 0x01)
    SUB = rffi.cast(MuFlag, 0x02)
    AND = rffi.cast(MuFlag, 0x03)
    NAND = rffi.cast(MuFlag, 0x04)
    OR = rffi.cast(MuFlag, 0x05)
    XOR = rffi.cast(MuFlag, 0x06)
    MAX = rffi.cast(MuFlag, 0x07)
    MIN = rffi.cast(MuFlag, 0x08)
    UMAX = rffi.cast(MuFlag, 0x09)
    UMIN = rffi.cast(MuFlag, 0x0A)
class MuCallConv:
    DEFAULT = rffi.cast(MuFlag, 0x00)
class MuCommInst:
    NEW_STACK = rffi.cast(MuFlag, 0x201)
    KILL_STACK = rffi.cast(MuFlag, 0x202)
    THREAD_EXIT = rffi.cast(MuFlag, 0x203)
    CURRENT_STACK = rffi.cast(MuFlag, 0x204)
    SET_THREADLOCAL = rffi.cast(MuFlag, 0x205)
    GET_THREADLOCAL = rffi.cast(MuFlag, 0x206)
    TR64_IS_FP = rffi.cast(MuFlag, 0x211)
    TR64_IS_INT = rffi.cast(MuFlag, 0x212)
    TR64_IS_REF = rffi.cast(MuFlag, 0x213)
    TR64_FROM_FP = rffi.cast(MuFlag, 0x214)
    TR64_FROM_INT = rffi.cast(MuFlag, 0x215)
    TR64_FROM_REF = rffi.cast(MuFlag, 0x216)
    TR64_TO_FP = rffi.cast(MuFlag, 0x217)
    TR64_TO_INT = rffi.cast(MuFlag, 0x218)
    TR64_TO_REF = rffi.cast(MuFlag, 0x219)
    TR64_TO_TAG = rffi.cast(MuFlag, 0x21a)
    FUTEX_WAIT = rffi.cast(MuFlag, 0x220)
    FUTEX_WAIT_TIMEOUT = rffi.cast(MuFlag, 0x221)
    FUTEX_WAKE = rffi.cast(MuFlag, 0x222)
    FUTEX_CMP_REQUEUE = rffi.cast(MuFlag, 0x223)
    KILL_DEPENDENCY = rffi.cast(MuFlag, 0x230)
    NATIVE_PIN = rffi.cast(MuFlag, 0x240)
    NATIVE_UNPIN = rffi.cast(MuFlag, 0x241)
    NATIVE_GET_ADDR = rffi.cast(MuFlag, 0x242)
    NATIVE_EXPOSE = rffi.cast(MuFlag, 0x243)
    NATIVE_UNEXPOSE = rffi.cast(MuFlag, 0x244)
    NATIVE_GET_COOKIE = rffi.cast(MuFlag, 0x245)
    META_ID_OF = rffi.cast(MuFlag, 0x250)
    META_NAME_OF = rffi.cast(MuFlag, 0x251)
    META_LOAD_BUNDLE = rffi.cast(MuFlag, 0x252)
    META_LOAD_HAIL = rffi.cast(MuFlag, 0x253)
    META_NEW_CURSOR = rffi.cast(MuFlag, 0x254)
    META_NEXT_FRAME = rffi.cast(MuFlag, 0x255)
    META_COPY_CURSOR = rffi.cast(MuFlag, 0x256)
    META_CLOSE_CURSOR = rffi.cast(MuFlag, 0x257)
    META_CUR_FUNC = rffi.cast(MuFlag, 0x258)
    META_CUR_FUNC_VER = rffi.cast(MuFlag, 0x259)
    META_CUR_INST = rffi.cast(MuFlag, 0x25a)
    META_DUMP_KEEPALIVES = rffi.cast(MuFlag, 0x25b)
    META_POP_FRAMES_TO = rffi.cast(MuFlag, 0x25c)
    META_PUSH_FRAME = rffi.cast(MuFlag, 0x25d)
    META_ENABLE_WATCHPOINT = rffi.cast(MuFlag, 0x25e)
    META_DISABLE_WATCHPOINT = rffi.cast(MuFlag, 0x25f)
    META_SET_TRAP_HANDLER = rffi.cast(MuFlag, 0x260)
    META_CONSTANT_BY_ID = rffi.cast(MuFlag, 0x268)
    META_GLOBAL_BY_ID = rffi.cast(MuFlag, 0x269)
    META_FUNC_BY_ID = rffi.cast(MuFlag, 0x26a)
    META_EXPFUNC_BY_ID = rffi.cast(MuFlag, 0x26b)
    IRBUILDER_NEW_IR_BUILDER = rffi.cast(MuFlag, 0x270)
    IRBUILDER_LOAD = rffi.cast(MuFlag, 0x300)
    IRBUILDER_ABORT = rffi.cast(MuFlag, 0x301)
    IRBUILDER_GEN_SYM = rffi.cast(MuFlag, 0x302)
    IRBUILDER_NEW_TYPE_INT = rffi.cast(MuFlag, 0x303)
    IRBUILDER_NEW_TYPE_FLOAT = rffi.cast(MuFlag, 0x304)
    IRBUILDER_NEW_TYPE_DOUBLE = rffi.cast(MuFlag, 0x305)
    IRBUILDER_NEW_TYPE_UPTR = rffi.cast(MuFlag, 0x306)
    IRBUILDER_NEW_TYPE_UFUNCPTR = rffi.cast(MuFlag, 0x307)
    IRBUILDER_NEW_TYPE_STRUCT = rffi.cast(MuFlag, 0x308)
    IRBUILDER_NEW_TYPE_HYBRID = rffi.cast(MuFlag, 0x309)
    IRBUILDER_NEW_TYPE_ARRAY = rffi.cast(MuFlag, 0x30a)
    IRBUILDER_NEW_TYPE_VECTOR = rffi.cast(MuFlag, 0x30b)
    IRBUILDER_NEW_TYPE_VOID = rffi.cast(MuFlag, 0x30c)
    IRBUILDER_NEW_TYPE_REF = rffi.cast(MuFlag, 0x30d)
    IRBUILDER_NEW_TYPE_IREF = rffi.cast(MuFlag, 0x30e)
    IRBUILDER_NEW_TYPE_WEAKREF = rffi.cast(MuFlag, 0x30f)
    IRBUILDER_NEW_TYPE_FUNCREF = rffi.cast(MuFlag, 0x310)
    IRBUILDER_NEW_TYPE_TAGREF64 = rffi.cast(MuFlag, 0x311)
    IRBUILDER_NEW_TYPE_THREADREF = rffi.cast(MuFlag, 0x312)
    IRBUILDER_NEW_TYPE_STACKREF = rffi.cast(MuFlag, 0x313)
    IRBUILDER_NEW_TYPE_FRAMECURSORREF = rffi.cast(MuFlag, 0x314)
    IRBUILDER_NEW_TYPE_IRBUILDERREF = rffi.cast(MuFlag, 0x315)
    IRBUILDER_NEW_FUNCSIG = rffi.cast(MuFlag, 0x316)
    IRBUILDER_NEW_CONST_INT = rffi.cast(MuFlag, 0x317)
    IRBUILDER_NEW_CONST_INT_EX = rffi.cast(MuFlag, 0x318)
    IRBUILDER_NEW_CONST_FLOAT = rffi.cast(MuFlag, 0x319)
    IRBUILDER_NEW_CONST_DOUBLE = rffi.cast(MuFlag, 0x31a)
    IRBUILDER_NEW_CONST_NULL = rffi.cast(MuFlag, 0x31b)
    IRBUILDER_NEW_CONST_SEQ = rffi.cast(MuFlag, 0x31c)
    IRBUILDER_NEW_CONST_EXTERN = rffi.cast(MuFlag, 0x31d)
    IRBUILDER_NEW_GLOBAL_CELL = rffi.cast(MuFlag, 0x31e)
    IRBUILDER_NEW_FUNC = rffi.cast(MuFlag, 0x31f)
    IRBUILDER_NEW_EXP_FUNC = rffi.cast(MuFlag, 0x320)
    IRBUILDER_NEW_FUNC_VER = rffi.cast(MuFlag, 0x321)
    IRBUILDER_NEW_BB = rffi.cast(MuFlag, 0x322)
    IRBUILDER_NEW_DEST_CLAUSE = rffi.cast(MuFlag, 0x323)
    IRBUILDER_NEW_EXC_CLAUSE = rffi.cast(MuFlag, 0x324)
    IRBUILDER_NEW_KEEPALIVE_CLAUSE = rffi.cast(MuFlag, 0x325)
    IRBUILDER_NEW_CSC_RET_WITH = rffi.cast(MuFlag, 0x326)
    IRBUILDER_NEW_CSC_KILL_OLD = rffi.cast(MuFlag, 0x327)
    IRBUILDER_NEW_NSC_PASS_VALUES = rffi.cast(MuFlag, 0x328)
    IRBUILDER_NEW_NSC_THROW_EXC = rffi.cast(MuFlag, 0x329)
    IRBUILDER_NEW_BINOP = rffi.cast(MuFlag, 0x32a)
    IRBUILDER_NEW_BINOP_WITH_STATUS = rffi.cast(MuFlag, 0x32b)
    IRBUILDER_NEW_CMP = rffi.cast(MuFlag, 0x32c)
    IRBUILDER_NEW_CONV = rffi.cast(MuFlag, 0x32d)
    IRBUILDER_NEW_SELECT = rffi.cast(MuFlag, 0x32e)
    IRBUILDER_NEW_BRANCH = rffi.cast(MuFlag, 0x32f)
    IRBUILDER_NEW_BRANCH2 = rffi.cast(MuFlag, 0x330)
    IRBUILDER_NEW_SWITCH = rffi.cast(MuFlag, 0x331)
    IRBUILDER_NEW_CALL = rffi.cast(MuFlag, 0x332)
    IRBUILDER_NEW_TAILCALL = rffi.cast(MuFlag, 0x333)
    IRBUILDER_NEW_RET = rffi.cast(MuFlag, 0x334)
    IRBUILDER_NEW_THROW = rffi.cast(MuFlag, 0x335)
    IRBUILDER_NEW_EXTRACTVALUE = rffi.cast(MuFlag, 0x336)
    IRBUILDER_NEW_INSERTVALUE = rffi.cast(MuFlag, 0x337)
    IRBUILDER_NEW_EXTRACTELEMENT = rffi.cast(MuFlag, 0x338)
    IRBUILDER_NEW_INSERTELEMENT = rffi.cast(MuFlag, 0x339)
    IRBUILDER_NEW_SHUFFLEVECTOR = rffi.cast(MuFlag, 0x33a)
    IRBUILDER_NEW_NEW = rffi.cast(MuFlag, 0x33b)
    IRBUILDER_NEW_NEWHYBRID = rffi.cast(MuFlag, 0x33c)
    IRBUILDER_NEW_ALLOCA = rffi.cast(MuFlag, 0x33d)
    IRBUILDER_NEW_ALLOCAHYBRID = rffi.cast(MuFlag, 0x33e)
    IRBUILDER_NEW_GETIREF = rffi.cast(MuFlag, 0x33f)
    IRBUILDER_NEW_GETFIELDIREF = rffi.cast(MuFlag, 0x340)
    IRBUILDER_NEW_GETELEMIREF = rffi.cast(MuFlag, 0x341)
    IRBUILDER_NEW_SHIFTIREF = rffi.cast(MuFlag, 0x342)
    IRBUILDER_NEW_GETVARPARTIREF = rffi.cast(MuFlag, 0x343)
    IRBUILDER_NEW_LOAD = rffi.cast(MuFlag, 0x344)
    IRBUILDER_NEW_STORE = rffi.cast(MuFlag, 0x345)
    IRBUILDER_NEW_CMPXCHG = rffi.cast(MuFlag, 0x346)
    IRBUILDER_NEW_ATOMICRMW = rffi.cast(MuFlag, 0x347)
    IRBUILDER_NEW_FENCE = rffi.cast(MuFlag, 0x348)
    IRBUILDER_NEW_TRAP = rffi.cast(MuFlag, 0x349)
    IRBUILDER_NEW_WATCHPOINT = rffi.cast(MuFlag, 0x34a)
    IRBUILDER_NEW_WPBRANCH = rffi.cast(MuFlag, 0x34b)
    IRBUILDER_NEW_CCALL = rffi.cast(MuFlag, 0x34c)
    IRBUILDER_NEW_NEWTHREAD = rffi.cast(MuFlag, 0x34d)
    IRBUILDER_NEW_SWAPSTACK = rffi.cast(MuFlag, 0x34e)
    IRBUILDER_NEW_COMMINST = rffi.cast(MuFlag, 0x34f)
    EXT_PRINT_STATS = rffi.cast(MuFlag, 0xc001)
    EXT_CLEAR_STATS = rffi.cast(MuFlag, 0xc002)

@always_inline
def ir_inst(inst, *args, **kwargs):
    pass

class Entry(ExtRegistryEntry):
    _about_ = ir_inst

    def compute_result_annotation(self, s_inst, *s_args, **s_kwds):
        s_restype = s_kwds.get('s_restype', None)
        if s_restype:
            assert s_restype.is_constant()
            LLT = s_restype.const
            if isinstance(LLT, lltype.Ptr):
                return lltype.SomePtr(s_restype.const)
            else:
                assert LLT == MuID, "Unanticipated result type: %(LLT)s" % locals()
                return lltype.SomeInteger(knowntype=LLT._type)
        return None

    def specialize_call(self, hop, **i_kwds):
        hop.exception_cannot_occur()
        inst = hop.args_v[0].value
        idx_kwds_start = len(hop.args_v) if len(i_kwds) == 0 else min(i_kwds.values())
        i_args = range(1, idx_kwds_start)
        args = [hop.inputconst(lltype.Void, inst)]
        for idx in i_args:
            args.append(hop.inputarg(hop.args_r[idx], idx))

        restype = hop.r_result.lowleveltype     # it's perhaps better to do it this way

        metainfo = {}
        args.append(hop.inputconst(lltype.Void, metainfo))

        i_opname = i_kwds.get('i_opname', None)
        opname = hop.args_v[i_opname].value if i_opname else 'mu_comminst'
        return hop.genop(opname, args, resulttype=restype)


def meta_id_of(name):
    name_irstr = str2metabytes(name)
    return ir_inst(MetaInst.ID_OF, name_irstr, restype=MuID)


def meta_name_of(id):
    return ir_inst(MetaInst.NAME_OF, id, restype=MuMetaBytes)

@specialize.arg(0)
def meta_const_by_id(TYPE, id):
    return ir_inst(MetaInst.CONSTANT_BY_ID, id, restype=TYPE, opname='mu_meta_xxx_by_id')  # further processing in ll2mu

@specialize.arg(0)
def meta_global_by_id(T, id):
    IREF = _iref_type_of(T)
    return ir_inst(MetaInst.GLOBAL_BY_ID, id, restype=IREF, opname='mu_meta_xxx_by_id')

@specialize.arg(0)
def meta_func_by_id(TYPE, id):
    return ir_inst(MetaInst.FUNC_BY_ID, id, restype=TYPE, opname='mu_meta_xxx_by_id')

@specialize.arg(0)
def meta_expfunc_by_id(TYPE, id):
    UFUNCPTR = _expfunc_type_of(TYPE)
    return ir_inst(MetaInst.EXPFUNC_BY_ID, id, restype=UFUNCPTR, opname='mu_meta_xxx_by_id')


class MuIRBuilder:
    def __init__(self):
        self._bldr = ir_inst(MetaIRBuilderInst.NEW_IR_BUILDER, restype=IRBuilderRef)

    def load(self):
        ir_inst(MetaIRBuilderInst.LOAD, self._bldr)

    def abort(self):
        ir_inst(MetaIRBuilderInst.ABORT, self._bldr)

    def gen_sym(self, name=None):
        _name = str2metacstr(name)
        return ir_inst(MetaIRBuilderInst.GEN_SYM, self._bldr, _name, restype=MuID)

    def new_type_int(self, id, len):
        len_c = rffi.cast(rffi.INT, len)
        ir_inst(MetaIRBuilderInst.NEW_TYPE_INT, self._bldr, id, len_c)

    def new_type_float(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_FLOAT, self._bldr, id)

    def new_type_double(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_DOUBLE, self._bldr, id)

    def new_type_uptr(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_UPTR, self._bldr, id, ty)

    def new_type_ufuncptr(self, id, sig):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_UFUNCPTR, self._bldr, id, sig)

    def new_type_struct(self, id, fieldtys):
        fieldtys_arr = lst2metacarr(MuID, fieldtys)
        fieldtys_sz = len(fieldtys)
        ir_inst(MetaIRBuilderInst.NEW_TYPE_STRUCT, self._bldr, id, fieldtys_arr, fieldtys_sz)

    def new_type_hybrid(self, id, fixedtys, varty):
        fixedtys_arr = lst2metacarr(MuID, fixedtys)
        fixedtys_sz = len(fixedtys)
        ir_inst(MetaIRBuilderInst.NEW_TYPE_HYBRID, self._bldr, id, fixedtys_arr, fixedtys_sz, varty)

    def new_type_array(self, id, elemty, len):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_ARRAY, self._bldr, id, elemty, len)

    def new_type_vector(self, id, elemty, len):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_VECTOR, self._bldr, id, elemty, len)

    def new_type_void(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_VOID, self._bldr, id)

    def new_type_ref(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_REF, self._bldr, id, ty)

    def new_type_iref(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_IREF, self._bldr, id, ty)

    def new_type_weakref(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_WEAKREF, self._bldr, id, ty)

    def new_type_funcref(self, id, sig):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_FUNCREF, self._bldr, id, sig)

    def new_type_tagref64(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_TAGREF64, self._bldr, id)

    def new_type_threadref(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_THREADREF, self._bldr, id)

    def new_type_stackref(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_STACKREF, self._bldr, id)

    def new_type_framecursorref(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_FRAMECURSORREF, self._bldr, id)

    def new_type_irbuilderref(self, id):
        ir_inst(MetaIRBuilderInst.NEW_TYPE_IRBUILDERREF, self._bldr, id)

    def new_funcsig(self, id, paramtys, rettys):
        paramtys_arr = lst2metacarr(MuID, paramtys)
        paramtys_sz = len(paramtys)
        rettys_arr = lst2metacarr(MuID, rettys)
        rettys_sz = len(rettys)
        ir_inst(MetaIRBuilderInst.NEW_FUNCSIG, self._bldr, id, paramtys_arr, paramtys_sz, rettys_arr, rettys_sz)

    def new_const_int(self, id, ty, value):
        ir_inst(MetaIRBuilderInst.NEW_CONST_INT, self._bldr, id, ty, value)

    def new_const_int_ex(self, id, ty, values):
        values_arr = lst2metacarr(rffi.ULONG, values)
        values_sz = len(values)
        ir_inst(MetaIRBuilderInst.NEW_CONST_INT_EX, self._bldr, id, ty, values_arr, values_sz)

    def new_const_float(self, id, ty, value):
        value_c = rffi.cast(rffi.FLOAT, value)
        ir_inst(MetaIRBuilderInst.NEW_CONST_FLOAT, self._bldr, id, ty, value_c)

    def new_const_double(self, id, ty, value):
        value_c = rffi.cast(rffi.DOUBLE, value)
        ir_inst(MetaIRBuilderInst.NEW_CONST_FLOAT, self._bldr, id, ty, value_c)

    def new_const_null(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_CONST_NULL, self._bldr, id, ty)

    def new_const_seq(self, id, ty, elems):
        elems_arr = lst2metacarr(MuID, elems)
        elems_sz = len(elems)
        ir_inst(MetaIRBuilderInst.NEW_CONST_SEQ, self._bldr, id, ty, elems_arr, elems_sz)

    def new_const_extern(self, id, ty, symbol):
        symbol_buf = str2metacstr(symbol)
        ir_inst(MetaIRBuilderInst.NEW_CONST_EXTERN, self._bldr, id, ty, symbol_buf)

    def new_global_cell(self, id, ty):
        ir_inst(MetaIRBuilderInst.NEW_GLOBAL_CELL, self._bldr, id, ty)

    def new_func(self, id, sig):
        ir_inst(MetaIRBuilderInst.NEW_FUNC, self._bldr, id, sig)

    def new_exp_func(self, id, func, callconv, cookie):
        ir_inst(MetaIRBuilderInst.NEW_EXP_FUNC, self._bldr, id, func, callconv, cookie)

    def new_func_ver(self, id, func, bbs):
        bbs_arr = lst2metacarr(MuID, bbs)
        bbs_sz = len(bbs)
        ir_inst(MetaIRBuilderInst.NEW_FUNC_VER, self._bldr, id, func, bbs_arr, bbs_sz)

    def new_bb(self, id, nor_param_ids, nor_param_types, exc_param_id, insts):
        nor_param_ids_arr = lst2metacarr(MuID, nor_param_ids)
        nor_param_ids_sz = len(nor_param_ids)
        nor_param_types_arr = lst2metacarr(MuID, nor_param_types)
        nor_param_types_sz = len(nor_param_types)
        insts_arr = lst2metacarr(MuID, insts)
        insts_sz = len(insts)
        ir_inst(MetaIRBuilderInst.NEW_BB, self._bldr, id, nor_param_ids_arr, nor_param_types_arr, nor_param_types_sz, exc_param_id, insts_arr, insts_sz)

    def new_dest_clause(self, id, dest, vars):
        vars_arr = lst2metacarr(MuID, vars)
        vars_sz = len(vars)
        ir_inst(MetaIRBuilderInst.NEW_DEST_CLAUSE, self._bldr, id, dest, vars_arr, vars_sz)

    def new_exc_clause(self, id, nor, exc):
        ir_inst(MetaIRBuilderInst.NEW_EXC_CLAUSE, self._bldr, id, nor, exc)

    def new_keepalive_clause(self, id, vars):
        vars_arr = lst2metacarr(MuID, vars)
        vars_sz = len(vars)
        ir_inst(MetaIRBuilderInst.NEW_KEEPALIVE_CLAUSE, self._bldr, id, vars_arr, vars_sz)

    def new_csc_ret_with(self, id, rettys):
        rettys_arr = lst2metacarr(MuID, rettys)
        rettys_sz = len(rettys)
        ir_inst(MetaIRBuilderInst.NEW_CSC_RET_WITH, self._bldr, id, rettys_arr, rettys_sz)

    def new_csc_kill_old(self, id):
        ir_inst(MetaIRBuilderInst.NEW_CSC_KILL_OLD, self._bldr, id)

    def new_nsc_pass_values(self, id, tys, vars):
        tys_arr = lst2metacarr(MuID, tys)
        tys_sz = len(tys)
        vars_arr = lst2metacarr(MuID, vars)
        vars_sz = len(vars)
        ir_inst(MetaIRBuilderInst.NEW_NSC_PASS_VALUES, self._bldr, id, tys_arr, vars_arr, vars_sz)

    def new_nsc_throw_exc(self, id, exc):
        ir_inst(MetaIRBuilderInst.NEW_NSC_THROW_EXC, self._bldr, id, exc)

    def new_binop(self, id, result_id, optr, ty, opnd1, opnd2, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_BINOP, self._bldr, id, result_id, optr, ty, opnd1, opnd2, exc_clause)

    def new_binop_with_status(self, id, result_id, status_result_ids, optr, status_flags, ty, opnd1, opnd2, exc_clause=MU_NO_ID):
        status_result_ids_arr = lst2metacarr(MuID, status_result_ids)
        status_result_ids_sz = len(status_result_ids)
        ir_inst(MetaIRBuilderInst.NEW_BINOP_WITH_STATUS, self._bldr, id, result_id, status_result_ids_arr, status_result_ids_sz, optr, status_flags, ty, opnd1, opnd2, exc_clause)

    def new_cmp(self, id, result_id, optr, ty, opnd1, opnd2):
        ir_inst(MetaIRBuilderInst.NEW_CMP, self._bldr, id, result_id, optr, ty, opnd1, opnd2)

    def new_conv(self, id, result_id, optr, from_ty, to_ty, opnd):
        ir_inst(MetaIRBuilderInst.NEW_CONV, self._bldr, id, result_id, optr, from_ty, to_ty, opnd)

    def new_select(self, id, result_id, cond_ty, opnd_ty, cond, if_true, if_false):
        ir_inst(MetaIRBuilderInst.NEW_SELECT, self._bldr, id, result_id, cond_ty, opnd_ty, cond, if_true, if_false)

    def new_branch(self, id, dest):
        ir_inst(MetaIRBuilderInst.NEW_BRANCH, self._bldr, id, dest)

    def new_branch2(self, id, cond, if_true, if_false):
        ir_inst(MetaIRBuilderInst.NEW_BRANCH2, self._bldr, id, cond, if_true, if_false)

    def new_switch(self, id, opnd_ty, opnd, default_dest, cases, dests):
        cases_arr = lst2metacarr(MuID, cases)
        cases_sz = len(cases)
        dests_arr = lst2metacarr(MuID, dests)
        dests_sz = len(dests)
        ir_inst(MetaIRBuilderInst.NEW_SWITCH, self._bldr, id, opnd_ty, opnd, default_dest, cases_arr, dests_arr, dests_sz)

    def new_call(self, id, result_ids, sig, callee, args, exc_clause=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        args_arr = lst2metacarr(MuID, args)
        args_sz = len(args)
        ir_inst(MetaIRBuilderInst.NEW_CALL, self._bldr, id, result_ids_arr, result_ids_sz, sig, callee, args_arr, args_sz, exc_clause, keepalive_clause)

    def new_tailcall(self, id, sig, callee, args):
        args_arr = lst2metacarr(MuID, args)
        args_sz = len(args)
        ir_inst(MetaIRBuilderInst.NEW_TAILCALL, self._bldr, id, sig, callee, args_arr, args_sz)

    def new_ret(self, id, rvs):
        rvs_arr = lst2metacarr(MuID, rvs)
        rvs_sz = len(rvs)
        ir_inst(MetaIRBuilderInst.NEW_RET, self._bldr, id, rvs_arr, rvs_sz)

    def new_throw(self, id, exc):
        ir_inst(MetaIRBuilderInst.NEW_THROW, self._bldr, id, exc)

    def new_extractvalue(self, id, result_id, strty, index, opnd):
        index_c = rffi.cast(rffi.INT, index)
        ir_inst(MetaIRBuilderInst.NEW_EXTRACTVALUE, self._bldr, id, result_id, strty, index_c, opnd)

    def new_insertvalue(self, id, result_id, strty, index, opnd, newval):
        index_c = rffi.cast(rffi.INT, index)
        ir_inst(MetaIRBuilderInst.NEW_INSERTVALUE, self._bldr, id, result_id, strty, index_c, opnd, newval)

    def new_extractelement(self, id, result_id, seqty, indty, opnd, index):
        ir_inst(MetaIRBuilderInst.NEW_EXTRACTELEMENT, self._bldr, id, result_id, seqty, indty, opnd, index)

    def new_insertelement(self, id, result_id, seqty, indty, opnd, index, newval):
        ir_inst(MetaIRBuilderInst.NEW_INSERTELEMENT, self._bldr, id, result_id, seqty, indty, opnd, index, newval)

    def new_shufflevector(self, id, result_id, vecty, maskty, vec1, vec2, mask):
        ir_inst(MetaIRBuilderInst.NEW_SHUFFLEVECTOR, self._bldr, id, result_id, vecty, maskty, vec1, vec2, mask)

    def new_new(self, id, result_id, allocty, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_NEW, self._bldr, id, result_id, allocty, exc_clause)

    def new_newhybrid(self, id, result_id, allocty, lenty, length, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_NEWHYBRID, self._bldr, id, result_id, allocty, lenty, length, exc_clause)

    def new_alloca(self, id, result_id, allocty, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_ALLOCA, self._bldr, id, result_id, allocty, exc_clause)

    def new_allocahybrid(self, id, result_id, allocty, lenty, length, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_ALLOCAHYBRID, self._bldr, id, result_id, allocty, lenty, length, exc_clause)

    def new_getiref(self, id, result_id, refty, opnd):
        ir_inst(MetaIRBuilderInst.NEW_GETIREF, self._bldr, id, result_id, refty, opnd)

    def new_getfieldiref(self, id, result_id, is_ptr, refty, index, opnd):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        index_c = rffi.cast(rffi.INT, index)
        ir_inst(MetaIRBuilderInst.NEW_GETFIELDIREF, self._bldr, id, result_id, is_ptr_c, refty, index_c, opnd)

    def new_getelemiref(self, id, result_id, is_ptr, refty, indty, opnd, index):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_GETELEMIREF, self._bldr, id, result_id, is_ptr_c, refty, indty, opnd, index)

    def new_shiftiref(self, id, result_id, is_ptr, refty, offty, opnd, offset):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_SHIFTIREF, self._bldr, id, result_id, is_ptr_c, refty, offty, opnd, offset)

    def new_getvarpartiref(self, id, result_id, is_ptr, refty, opnd):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_GETVARPARTIREF, self._bldr, id, result_id, is_ptr_c, refty, opnd)

    def new_load(self, id, result_id, is_ptr, ord, refty, loc, exc_clause=MU_NO_ID):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_LOAD, self._bldr, id, result_id, is_ptr_c, ord, refty, loc, exc_clause)

    def new_store(self, id, is_ptr, ord, refty, loc, newval, exc_clause=MU_NO_ID):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_STORE, self._bldr, id, is_ptr_c, ord, refty, loc, newval, exc_clause)

    def new_cmpxchg(self, id, value_result_id, succ_result_id, is_ptr, is_weak, ord_succ, ord_fail, refty, loc, expected, desired, exc_clause=MU_NO_ID):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        is_weak_c = rffi.cast(rffi.INT, is_weak)
        ir_inst(MetaIRBuilderInst.NEW_CMPXCHG, self._bldr, id, value_result_id, succ_result_id, is_ptr_c, is_weak_c, ord_succ, ord_fail, refty, loc, expected, desired, exc_clause)

    def new_atomicrmw(self, id, result_id, is_ptr, ord, optr, ref_ty, loc, opnd, exc_clause=MU_NO_ID):
        is_ptr_c = rffi.cast(rffi.INT, is_ptr)
        ir_inst(MetaIRBuilderInst.NEW_ATOMICRMW, self._bldr, id, result_id, is_ptr_c, ord, optr, ref_ty, loc, opnd, exc_clause)

    def new_fence(self, id, ord):
        ir_inst(MetaIRBuilderInst.NEW_FENCE, self._bldr, id, ord)

    def new_trap(self, id, result_ids, rettys, exc_clause=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        rettys_arr = lst2metacarr(MuID, rettys)
        rettys_sz = len(rettys)
        ir_inst(MetaIRBuilderInst.NEW_TRAP, self._bldr, id, result_ids_arr, rettys_arr, rettys_sz, exc_clause, keepalive_clause)

    def new_watchpoint(self, id, wpid, result_ids, rettys, dis, ena, exc=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        rettys_arr = lst2metacarr(MuID, rettys)
        rettys_sz = len(rettys)
        ir_inst(MetaIRBuilderInst.NEW_WATCHPOINT, self._bldr, id, wpid, result_ids_arr, rettys_arr, rettys_sz, dis, ena, exc, keepalive_clause)

    def new_wpbranch(self, id, wpid, dis, ena):
        ir_inst(MetaIRBuilderInst.NEW_WPBRANCH, self._bldr, id, wpid, dis, ena)

    def new_ccall(self, id, result_ids, callconv, callee_ty, sig, callee, args, exc_clause=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        args_arr = lst2metacarr(MuID, args)
        args_sz = len(args)
        ir_inst(MetaIRBuilderInst.NEW_CCALL, self._bldr, id, result_ids_arr, result_ids_sz, callconv, callee_ty, sig, callee, args_arr, args_sz, exc_clause, keepalive_clause)

    def new_newthread(self, id, result_id, stack, threadlocal, new_stack_clause, exc_clause=MU_NO_ID):
        ir_inst(MetaIRBuilderInst.NEW_NEWTHREAD, self._bldr, id, result_id, stack, threadlocal, new_stack_clause, exc_clause)

    def new_swapstack(self, id, result_ids, swappee, cur_stack_clause, new_stack_clause, exc_clause=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        ir_inst(MetaIRBuilderInst.NEW_SWAPSTACK, self._bldr, id, result_ids_arr, result_ids_sz, swappee, cur_stack_clause, new_stack_clause, exc_clause, keepalive_clause)

    def new_comminst(self, id, result_ids, opcode, flags, tys, sigs, args, exc_clause=MU_NO_ID, keepalive_clause=MU_NO_ID):
        result_ids_arr = lst2metacarr(MuID, result_ids)
        result_ids_sz = len(result_ids)
        flags_arr = lst2metacarr(MuID, flags)
        flags_sz = len(flags)
        tys_arr = lst2metacarr(MuID, tys)
        tys_sz = len(tys)
        sigs_arr = lst2metacarr(MuID, sigs)
        sigs_sz = len(sigs)
        args_arr = lst2metacarr(MuID, args)
        args_sz = len(args)
        ir_inst(MetaIRBuilderInst.NEW_COMMINST, self._bldr, id, result_ids_arr, result_ids_sz, opcode, flags_arr, flags_sz, tys_arr, tys_sz, sigs_arr, sigs_sz, args_arr, args_sz, exc_clause, keepalive_clause)

# ----------------
# Helper functions

_StrBuf = lltype.GcArray(lltype.Char, hints={'nolength': True, 'mu_nohashfield': True})
_StrBufPtr = lltype.Ptr(_StrBuf)
MuMetaCStr = lltype.Ptr(lltype.GcOpaqueType('MuMetaCStr',
                                            hints={'mu_render_ptr_as': mutype.META_CSTR}))
_BytesBuf = lltype.GcArray(lltype.Char, hints={'mu_nohashfield': True,
                                               'mu_render_as': mutype.META_BYTES})
MuMetaBytes = lltype.Ptr(_BytesBuf)

def str2metacstr(str=None):
    if str:
        n = len(str)
        buf = lltype.malloc(_StrBuf, n + 1)
        adr_buf = llmemory.cast_ptr_to_adr(buf) + llmemory.itemoffsetof(_StrBuf, 0)
        adr_src = llmemory.cast_ptr_to_adr(str) + (llmemory.offsetof(rstr.STR, 'chars') +
                                                   llmemory.itemoffsetof(rstr.STR.chars, 0) +
                                                   llmemory.sizeof(rstr.Char) * 0)
        llmemory.raw_memcopy(adr_src, adr_buf, llmemory.sizeof(rstr.Char) * n)
        buf[n] = '\x00'
        keepalive_until_here(buf)
        keepalive_until_here(str)
        ircstr = llop.mu_meta_barebuf2cstriref(MuMetaCStr, buf)     # handled in ll2mu
        return ircstr
    else:
        return lltype.nullptr(MuMetaCStr.TO)


def str2metabytes(str=None):
    if str:
        n = len(str)
        buf = lltype.malloc(_BytesBuf, n)   # length should have been set in ll2mu
        adr_buf = llmemory.cast_ptr_to_adr(buf) + llmemory.itemoffsetof(_BytesBuf, 0)
        adr_src = llmemory.cast_ptr_to_adr(str) + (llmemory.offsetof(rstr.STR, 'chars') +
                                                   llmemory.itemoffsetof(rstr.STR.chars, 0) +
                                                   llmemory.sizeof(rstr.Char) * 0)
        llmemory.raw_memcopy(adr_src, adr_buf, llmemory.sizeof(rstr.Char) * n)
        keepalive_until_here(buf)
        keepalive_until_here(str)
        return buf
    else:

        return lltype.nullptr(MuMetaBytes.TO)
@specialize.memo()
def _carriptr_type_of(ELM_T):
    ELM_MuT = ll2mu_type(ELM_T)
    return lltype.Ptr(lltype.GcOpaqueType('itemarray', hints={'mu_render_ptr_as': mutype.MuIRef(ELM_MuT)}))

@specialize.memo()
def _iref_type_of(T):
    T_MuT = ll2mu_type(T)
    return lltype.Ptr(lltype.GcOpaqueType('iref', hints={'mu_render_ptr_as': mutype.MuIRef(T_MuT)}))

@specialize.memo()
def _expfunc_type_of(TYPE):
    Sig = ll2mu_type(TYPE).Sig
    return lltype.Ptr(lltype.GcOpaqueType('ufuncptr', hints={'mu_render_ptr_as': mutype.MuUFuncPtr(Sig)}))

def lst2metacarr(ELM_T, lst):
    IPtrBuf = _carriptr_type_of(ELM_T)
    return llop.mu_meta_lst2carr(IPtrBuf, lst)  # handled in ll2mu
